# NHPlus

# Hinzugefügte Erweiterungen aufgrund von Datenschutzgründen
1.	Das Feld „Vermögensstand" aus der Tabelle entfernen
2.	Das Löschen der Patientendaten erst nach 10 Jähriger Aufbewahrung möglich machen. 
3.	Alle Pfleger die beim Pflegeheim arbeiten und Behandlungen durchführen erfassen
4.	Benutzeranmeldung mit Passwort einrichten

Zu 1: In der Klasse "AllPatienController" wurden alle Verweise auf das Atribut Vermögensstand entfernt. In der Klasse "Patient" wurden alle Verweise auf das Attribut Vermögensstand entfernt.
In der Klasse "PatientDAO" wurden alle Verweise auf das Attribut Vermgensstand entfernt. In der Datenbank "Patient" muss die Spalte "Vermögensstand" entfernt werden.

Zu 2: 
In der Klasse „AllPatientController“ wurde die Funktion „handleDeleteRow“ erweitert. Hier wurde eine Abfrage hinzugefügt, bei der die neu hinzugefügte Funktion „treatmentOlderThanTenYears“ aufgerufen wird. Die Funktion prüft welche Behandlung die letzte war und ob diese 10 Jahre alt ist, oder nicht. Die Daten werden aus der Datenbank gezogen, hierbei wurde in der Klasse „TreatmentDAO“ die funktion „readTreatmentsByPid „hinzugefügt hier wird die Patienten ID übergeben und alle Behandlungen des Patienten mittels einer SQL Abfrage in einer ArrayList gespeichert, der SQL Befehl wurde in der Funktion „getReadAllTreatmentsOfOnePatientByPid“ festgelegt, die Funktion benötigt die PatientenID und gibt den SQL Befehl als String zurück.

Zu 3: Die Datenbank Caregiver wurde hinzugeüfgt, sowie die Klasse "AllCaregiverController" und "CaregiverDAO"

Zu 4: 
Die Klasse "LoginController" wurde hinzugefügt, hier wird die eingegebene ID und das Passwort mit der Datenbank verglichen. Außerdem wurde das LoginView hinzugefügt, hier wird der LogIn dargestellt. Das Passwort wird in der Datenbank verschlüsselt.

# Probleme die noch nicht behoben sind

Beim Löschen eines Pflegers wird noch nicht geprüft ob derzeit ein Admin eingeloggt ist.
Vor dem LogIn sind die Buttons für die Patientendaten, Behandlungen und Pflegerdaten noch Funktionierbar, diese sollen eigentlich disabled werden und erst enabled wenn der LogIn Erfolgreich war. 
Während ein Benutzer eingeloggt ist fehlt noch der LogOut, außerdem sollte oben Links der eingeloggte Pfleger/in angezeigt werden.



## Technische Hinweise

Wird das Open JDK verwendet, werden JavaFX-Abhängigkeiten nicht importiert. Die Lösung besteht in der Installation der neuesten JDK-Version der Firma Oracle.

## Technische Hinweise zur Datenbank

- Benutzername: SA
- Passwort: SA
- Bitte nicht in die Datenbank schauen, während die Applikation läuft. Das sorgt leider für einen Lock, der erst wieder verschwindet, wenn IntelliJ neugestartet wird!