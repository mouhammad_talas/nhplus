package controller;

import datastorage.CaregiverDAO;
import datastorage.DAOFactory;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javax.swing.*;



public class LoginController {

    @FXML
    TextField txtIDLI;

    @FXML
    PasswordField pswPasswordLI;

    @FXML
    Button bnSubmitLI;

    private CaregiverDAO dao;

    private int caregiverId;

    private String password;

    CryptoController cryptoController = new CryptoController();





    public boolean validateLogin () throws Exception {

        this.dao = DAOFactory.getDAOFactory().createCaregiverDAO();

        password = pswPasswordLI.getText();
        caregiverId = Integer.parseInt(txtIDLI.getText());

        String encryptedPassword = dao.readPasswordById(caregiverId);

        if (cryptoController.decrypt(encryptedPassword).equals(password))
        {
            pswPasswordLI.setDisable(true);
            txtIDLI.setDisable(true);
            return true;
        }
        else
            return false;


    }

    public void checkTheLogin() throws Exception {
        if(!validateLogin())
        {
            JOptionPane.showMessageDialog(null,"Falsche ID oder Passwort");
        }
        else
            {
                JOptionPane.showMessageDialog(null,"Erfolgreich eingeloggt");

            }

    }



}
