package datastorage;

import controller.CryptoController;
import model.Caregiver;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Implements the Interface <code>DAOImp</code>. Overrides methods to generate specific caregiver-SQL-queries.
 */
public class CaregiverDAO extends DAOimp<Caregiver> {

    CryptoController cryptoController = new CryptoController();

    /**
     * constructs Onbject. Calls the Constructor from <code>DAOImp</code> to store the connection.
     * @param
     */
    public CaregiverDAO(Connection conn) {
        super(conn);
    }
    /**
     * generates a <code>INSERT INTO</code>-Statement for a given patient
     * @param caregiver for which a specific INSERT INTO is to be created
     * @return <code>String</code> with the generated SQL.
     */
    @Override
    protected String getCreateStatementString(Caregiver caregiver) throws Exception {

        String password = cryptoController.encrypt(caregiver.getPassword());

        return String.format("INSERT INTO caregiver (firstname, surname, telephoneNumber, password, adminRight) VALUES ('%s', '%s', '%s', '%s', '%s')",
            caregiver.getFirstName(), caregiver.getSurname(), caregiver.getTelephone(),password, caregiver.getAdminRight());
    }
    /**
     * generates a <code>select</code>-Statement for a given key
     * @param key for which a specific SELECTis to be created
     * @return <code>String</code> with the generated SQL.
     */
    @Override
    protected String getReadByIDStatementString(int key) {
        return String.format("SELECT * FROM caregiver WHERE caregiverId = %d", key);
    }

    @Override
    protected Caregiver getInstanceFromResultSet(ResultSet set) throws SQLException {
        Caregiver c = null;
        c = new Caregiver(set.getString(1), set.getString(2),
                set.getString(3), set.getString(4), set.getString(5));
        return c;
    }
    /**
     * generates a <code>SELECT</code>-Statement for all patients.
     * @return <code>String</code> with the generated SQL.
     */
    @Override
    protected String getReadAllStatementString() {
        return "SELECT * FROM caregiver";
    }

    @Override
    protected ArrayList<Caregiver> getListFromResultSet(ResultSet set) throws SQLException {
        ArrayList<Caregiver> list = new ArrayList<Caregiver>();
        Caregiver c = null;
        while (set.next()) {
            c = new Caregiver(set.getLong(1), set.getString(2),
                    set.getString(3), set.getString(4));
            list.add(c);
        }
        return list;
    }
    /**
     * generates a <code>UPDATE</code>-Statement for a given patient
     * @param caregiver for which a specific update is to be created
     * @return <code>String</code> with the generated SQL.
     */
    @Override
    protected String getUpdateStatementString(Caregiver caregiver) {
        return String.format("UPDATE caregiver SET firstname = '%s', surname = '%s' " +
                        "telephoneNumber = '%s' WHERE caregiverId = %d", caregiver.getFirstName(), caregiver.getSurname(),
                caregiver.getTelephone(), caregiver.getCaregiverId());
    }
    /**
     * generates a <code>delete</code>-Statement for a given key
     * @param key for which a specific DELETE is to be created
     * @return <code>String</code> with the generated SQL.
     */
    @Override
    protected String getDeleteStatementString(int key) {
        return String.format("Delete FROM caregiver WHERE caregiverId=%d", key);
    }

    public void deleteById(int key) throws SQLException {
        Statement st = conn.createStatement();
        st.executeUpdate(String.format("Delete FROM caregiver WHERE caregiverId= %d", key));
    }

    public String getReadPasswordById(int key)  {

         return  String.format("Select password FROM caregiver WHERE caregiverId=%d", key);
    }

    public String readPasswordById(int key) throws SQLException {
        String p = null;

        Statement st = conn.createStatement();
        ResultSet result = st.executeQuery(getReadPasswordById(key));

        while(result.next())
        {
           p = result.getString(1);
        }
      return p ;

    }

    public Caregiver read(int key) throws SQLException {
        Caregiver c = null;
        Statement st = conn.createStatement();
        ResultSet result = st.executeQuery(getReadByIDStatementString(key));
        if (result.next()) {
            c = getInstanceFromResultSet(result);
        }
        return c;
    }


     // test if the logged in Account Admin or not
     //return boolean


    //public boolean testAdminState(int key) throws SQLException
    //{
    //Statement st= conn.createStatement();
    // ResultSet result = st.executeQuery(getAdminState(key));
    //  if(result.getString(5) == "Nein")
    //      return false;
    //   else
    //      {
    //      return true;
    //   }
    // }


    // * select the admin state of one caregiver
    //* return SQL String
    // *
    // public String getAdminState(int key)
    //{
    //  return String.format("SELECT adminRight FROM caregiver WHERE caregiverId= %d", key);
    // }

}
