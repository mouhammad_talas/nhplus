package model;

/**
 * Caregiver treating the Patients.
 */
public class Caregiver extends Person {
    private long caregiverId;
    private String telephone;
    private String password;
    private String adminRight;

    /**
     * @param firstName
     * @param surname
     * @param telephone
     * @param password
     * @param adminRight
     */

    public Caregiver(String firstName, String surname, String telephone,String password, String adminRight) {
        super(firstName, surname);
        this.telephone = telephone;
        this.password = password;
        this.adminRight = adminRight;
    }

    /**
     * constructs a Caregiver from the given params.
     * @param caregiverId;
     * @param firstName
     * @param surname
     * @param telephone
     */
    public Caregiver(long caregiverId, String firstName, String surname, String telephone) {
        super(firstName, surname);
        this.telephone = telephone;
        this.caregiverId = caregiverId;
    }

    /**
     *
     * @return caregiver id
     */

    public long getCaregiverId() {
        return caregiverId;
    }

    /**
     *
     * @return telephone as a string
     * */
    public String getTelephone() {
        return telephone;
    }
    /**
     *
     * @param telephone new telephone number
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }
    /**
     *
     * @return Password as string
     */
    public String getPassword() {
        return password;
    }
    /**
     *
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }
    /**
     *
     * @return AdminRight as string
     */
    public String getAdminRight() {
        return adminRight;
    }
    /**
     *
     * @param adminRight
     */

    public void setAdminRight(String adminRight) {
        this.adminRight = adminRight;
    }
    /**
     *
     * @return string-representation of the caregiver
     */
    public String toString() {
        return "Caregiver" + "\nMNID: " + this.caregiverId +
                "\nFirstname: " + this.getFirstName() +
                "\nSurname: " + this.getSurname() +
                "\nTelephone: " + this.telephone +
                "\nPassword: " + this.password +
                "\nAdmin Right: " + this.adminRight +
                "\n";
    }
}
